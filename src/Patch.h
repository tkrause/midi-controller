// Patch.h

#ifndef _PATCH_h
#define _PATCH_h

#include <string.h>
#include "config.h"
#include "Color.h"

// Available flags for the program change
const byte PATCH_FLAG_IS_PROGRAM_CHANGE = 1 << 0;
const byte PATCH_FLAG_EXPRESSION_PEDAL_1 = 1 << 1;
const byte PATCH_FLAG_EXPRESSION_PEDAL_2 = 1 << 2;

class Patch
{
protected:
	/**
	 * The name of the patch
	 */
	char name[MAX_NAME_LEN] = { 0 };

	/**
	 * The midi channels we should send the messages
	 * on
	 */
	unsigned short channels = 0;

	/**
	 * Settings for this patch
	 */
	byte flags = 0;

	/**
	 * The midi control number
	 */
//	byte control = 0;

	/**
	 * Color states while on and off
	 */
	Color colorActive;
	Color colorInactive;

public:
	Patch* setName(const char* name)
	{
		clearName();

		const size_t len = strlen(name);
		for (size_t i = 0; i < len; ++i)
		{
			if (i < MAX_NAME_LEN)
				this->name[i] = name[i];
		}

		return this;
	}

	Patch* clearName()
	{
		for (char &i : this->name)
			i = 0;

		return this;
	}

	const char* getName() const
	{
		return this->name;
	}

	Patch* setActiveColor(const Color& c)
	{
		this->colorActive = c;

		return this;
	}

	Patch* setInactiveColor(const Color& c)
	{
		this->colorInactive = c;

		return this;
	}

	Patch* setChannels(const unsigned short& channels)
	{
		this->channels = channels;

		return this;
	}

	Patch* setChannel(const unsigned short& id, const bool& state)
	{
		if (state)
			this->channels |= 1 << id;
		else
			this->channels &= ~(1 << id);

		return this;
	}

	Patch* setFlag(const byte& flag, const bool& state)
	{
		if (state)
			this->flags |= flag;
		else
			this->flags &= ~flag;

		return this;
	}

	Color& getColorActive()
	{
		return this->colorActive;
	}

	Color& getColorInactive()
	{
		return this->colorInactive;
	}

    const bool isProgramChange() const
    {
        return this->hasFlag(PATCH_FLAG_IS_PROGRAM_CHANGE);
    }

	byte getFlags() const {
		return flags;
	}

    const bool hasFlag(byte mask) const
    {
        return (bool)(this->getFlags() & mask);
    }

	unsigned short getChannels() const {
		return channels;
	}

	Patch* clear()
	{
		this->clearName();
		this->colorActive.setColor(0, 0, 0);
		this->colorInactive.setColor(0, 0, 0);
		this->channels = 0;
		this->flags = 0;

		return this;
	}

	bool hasChannels() const
	{
		return this->getChannels() != 0;
	}
};

#endif

