// Bank.h

#ifndef _BANK_h
#define _BANK_h

#include "Patch.h"
#include "Glyph.h"

class Bank
{
protected:
	// Array of patches
	Patch patches[PATCHES_PER_BANK];
	// String name
	char name[MAX_NAME_LEN] = { 0 };

	// Custom Characters for this patch
//    Glyph glyphs[MAX_CUSTOM_CHARS] {};

public:
	Bank* setPatch(unsigned int index, const Patch patch)
	{
		if (index < PATCHES_PER_BANK)
			this->patches[index] = patch;

		return this;
	}

	Bank* setName(const char* name)
	{
		const size_t len = strlen(name);
		for (size_t i = 0; i < len; ++i)
		{
			if (i < MAX_NAME_LEN)
				this->name[i] = name[i];
		}

		return this;
	}

	Patch* getPatch(unsigned int index)
	{
		if (index < PATCHES_PER_BANK)
			return &this->patches[index];
		return nullptr;
	}

	Patch* getPatches()
	{
		return this->patches;
	}

	const char* getName() const
	{
		return this->name;
	}

	Bank* clear()
	{
		this->clearName();
		this->clearPatches();

		return this;
	}

	Bank* clearPatches()
	{
		for (Patch& p : this->patches)
			p.clear();

		return this;
	}

	Bank* clearName()
	{
		for (char &i : this->name)
			i = 0;

		return this;
	}

	const bool hasName() const
	{
		return name[0] != '\0';
	}

	const void display(Print& device) const
	{
		if (hasName())
			device.println(this->name);
		else
			device.println("Unnamed Bank");
	}

};

#endif

