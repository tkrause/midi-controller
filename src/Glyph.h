#ifndef MIDI_CONTROLLER_CUSTOMCHAR_H
#define MIDI_CONTROLLER_CUSTOMCHAR_H

#include <stdint.h>

extern const unsigned int MAX_CUSTOM_CHARS;

// 5x8 matrix of bits
struct Glyph
{
    byte data[8] = { 0x00 };
};

#endif //MIDI_CONTROLLER_CUSTOMCHAR_H
