#ifndef MIDI_CONTROLLER_GLOBAL_H
#define MIDI_CONTROLLER_GLOBAL_H

#include "config.h"
#include <LiquidCrystal.h>

static LiquidCrystal lcd(rs, en, d4, d5, d6, d7);

#endif
