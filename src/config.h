// config.h

#ifndef _CONFIG_h
#define _CONFIG_h

// Device configuration

const unsigned int MAX_BANKS = 5;
const unsigned int MAX_NAME_LEN = 20;
// Usually this is 8, some displays can support more
const unsigned int MAX_CUSTOM_CHARS = 8;

const unsigned int PATCHES_PER_BANK = 8;
const unsigned int PATCH_BTN_START_PIN = 22;

// First bank is at 0
const unsigned int DEFAULT_BANK = 0;

const unsigned int BANK_UP_PIN = 32;
const unsigned int BANK_DOWN_PIN = 33;
/**
 * LCD Pins
 */
const unsigned int rs = 49, en = 48, d4 = 47, d5 = 46, d6 = 45, d7 = 44;

/**
 * Storage Config
 */
const int STORAGE_DISK = 0x50;
const unsigned long STORAGE_SIZE = 65536; // 64 KB

/**
 * Protocol Config
 */
//const char VENDOR_ID[] = { 0x7D };

/**
 * ShiftPWM Config
 */

const int ShiftPWM_latchPin=2;
// ** uncomment this part to NOT use the SPI port and change the pin numbers. This is 2.5x slower **
// #define SHIFTPWM_NOSPI
// const int ShiftPWM_dataPin = 11;
// const int ShiftPWM_clockPin = 13;


// If your LED's turn on if the pin is low, set this to true, otherwise set it to false.
const bool ShiftPWM_invertOutputs = true;

// You can enable the option below to shift the PWM phase of each shift register by 8 compared to the previous.
// This will slightly increase the interrupt load, but will prevent all PWM signals from becoming high at the same time.
// This will be a bit easier on your power supply, because the current peaks are distributed.
const bool ShiftPWM_balanceLoad = false;

#endif

