#ifndef MIDI_CONTROLLER_MIDICONTROLLER_H
#define MIDI_CONTROLLER_MIDICONTROLLER_H

#include <ShiftPWM.h>
#include <HardwareSerial.h>
#include <ArduinoJson.h>
#include "global.h"
#include "helper.h"
#include "Bank.h"
#include "IO/InputManager.h"
#include "Protocol/Commands.h"
#include "Protocol/Protocol.h"

using namespace IO;

void onPingHandler(Event* data);
void onModeSetHandler(Event* data);
void onModeQueryHandler(Event* data);
void onSettingsQueryHandler(Event* data);
void onPatchQueryHandler(Event* data);
void onPatchSetHandler(Event* data);

class MidiController
{
protected:
    /**
     * The bank configuration
     */
    Bank bank;

    /**
     * The current bank
     */
    unsigned int current = 0;

    /**
     * The state of each of the patches
     */
    bool patchStates[PATCHES_PER_BANK] = { false };

    /**
     * The overall mode that the controller is operating in
     */
    byte mode = 0;

    /**
     * The time stamp that we last received a ping
     */
    unsigned long lastPing = 0;

public:
    static const PROGMEM char MODE_RUN = 0;
    static const PROGMEM char MODE_SOCKET = 1;
    static const PROGMEM char MODE_PROGRAM = 2;
    static const PROGMEM char MODE_PROGRAM_SERIAL = 3;

    void init()
    {
        Protocol.begin(Serial, 0x10, 10);

        Protocol.on(CMD_PING, &onPingHandler);
        Protocol.on(CMD_MODE_SET, &onModeSetHandler);
        Protocol.on(CMD_MODE_QUERY, &onModeQueryHandler);
        Protocol.on(CMD_SETTINGS_QUERY, &onSettingsQueryHandler);
        Protocol.on(CMD_PATCH_QUERY, &onPatchQueryHandler);
        Protocol.on(CMD_PATCH_SET, &onPatchSetHandler);
    }

    void start(bool def = true) {
        lcd.clear();

        int bank = def ? DEFAULT_BANK : current;

        this->loadBank(bank)
            ->display(lcd);
    }

    Bank* getBank()
    {
        return &bank;
    }

    const Bank* nextBank()
    {
        // Load the next bank
        return loadBank(++current);
    }

    const Bank* prevBank()
    {
        // Load the previous bank
        return loadBank(--current);
    }

    // TODO: on bank change, don't overwrite patch states or save them somewhere in case they are just browsing banks
    const Bank* loadBank(unsigned int index)
    {
        // If we've reached the end of our banks
        // reset it back to the first bank
        if (index >= MAX_BANKS)
            index = 0;

        if (index < 0)
            index = MAX_BANKS - 1;

        current = index;

        // Load the bank into memory
        Helper::read<Bank>(STORAGE_DISK, (unsigned int) getOffset(index), bank);

        clearPatches();

        return getBank();
    }

    const int getOffset(int index)
    {
        return (sizeof(Bank) * index);
    }

    const int getCurrentBank() const
    {
        return this->current;
    }

    bool writeBank()
    {
        return writeBank(bank);
    }

    bool writeBank(Bank bank)
    {
        if (current < MAX_BANKS) {
            Helper::write<Bank>(STORAGE_DISK, (unsigned int) getOffset(current), bank);
            return true;
        }

        return false;
    }

    MidiController* clearPatches()
    {
        for (unsigned int i = 0; i < PATCHES_PER_BANK; ++i) {
            setPatch(i, false);
        }

        return this;
    }

    Patch* togglePatch(unsigned int index)
    {
        if (index >= PATCHES_PER_BANK)
            return nullptr;

        Patch* patch = bank.getPatch(index);
        bool state = patchStates[index];

        // If program change type, only handle if the state is currently low (i.e we aren't on this program)
        // if we need to change the program then unset any other patches that are programs
        if (patch->isProgramChange() && ! state) {
            setPatch(index, true);

            // Unset any old patches that were programs
            for (unsigned int i = 0; i < PATCHES_PER_BANK; ++i) {
                if (i != index && getBank()->getPatch(i)->isProgramChange()) {
                    setPatch(i, false);
                }
            }
        } else if (! patch->isProgramChange()) {
            // Toggle the state of the patch
            setPatch(index, ! patchStates[index]);
        }

        return patch;
    }

    void setPatch(unsigned int index, bool state)
    {
        if (index >= PATCHES_PER_BANK)
            return;

        Patch* patch = getBank()->getPatch(index);

        // Is the patch enabled? i.e does it have any channels?
        // If not, we aren't using this patch and it should be disabled
        if (! patch->hasChannels()) {
            this->setPatchState(index, Color(0, 0, 0), false);
            return;
        }

        if (! isProgramming()) {
            // 16 possible channels, check them all and send
            // messages for enabled channels
            unsigned short b = patch->getChannels();
            for (byte i = 0; b > 0; ++i, b >>= 1) {
                // If this channel is disabled, skip
                if (! (b & 1))
                    continue;

                // Is this a program change?
                // if so, send a bank change message
                if (patch->isProgramChange()) {

                    // Send Program Change.
                    // Program changes should only be toggles
                    if (! state)
                        continue;

                    Serial.write(0xC0 + i); // Type and Channel
                    Serial.write(index); // Program #
                } else {
                    // Send a Control Change message
                    Serial.write(0xB0 + i); // Type and Channel
                    Serial.write(0x00); // Message Type 0X07 = Volume, 0x00 = Bank
                    Serial.write(0); // Value 0-127
                }
            }
        }

        Color c = state ? patch->getColorActive() : patch->getColorInactive();
        this->setPatchState(index, c, state);
    }

    /**
     * Set the state of the patch and LED
     *
     * @param index
     * @param state
     */
    void setPatchState(int index, Color color, bool state)
    {
        ShiftPWM.SetRGB(index, color.getRed(), color.getGreen(), color.getBlue());
        patchStates[index] = state;
    }

    /**
     * Sets the running mode of the controller
     * @param mode
     */
    void setMode(byte mode)
    {
        this->mode = mode;

        // Disable inputs in Programming mode
        if (isProgramming()) {
            // Acknowledge the mode switch
//            Protocol.ack(mode);

            lcd.clear();
            lcd.print(F("PROGRAMMING!"));
            lcd.setCursor(0, 1);
            lcd.print(F("DO NOT SHUT OFF"));

            // Shutdown all the lights
            ShiftPWM.SetAllRGB(0, 0, 0);
        }

        Inputs.setDisabled(isProgramming());

        if (mode == MODE_RUN) {
            this->start();
        }

        if (mode == MODE_SOCKET) {
            this->start(true);
        }
    }

    bool isProgramming()
    {
        return this->mode == MODE_PROGRAM_SERIAL || this->mode == MODE_PROGRAM;
    }

    /**
     * Do anything required every tick
     */
    void loop()
    {
        Protocol.process();

        // If we're in programming mode, handle that
        if (isProgramming()) {
            program();
            return;
        }
    }

    /**
     * Loop while in programming mode
     */
    void program()
    {
        // If we're in programming mode and the socket times out
        // set it back to run mode
        if (millis() - lastPing > 10000) {
            // Disconnect the socket
            setMode(MODE_RUN);
        }
    }

    //region Events

    void onPing(const Event* e)
    {
        lastPing = millis();

        // Set mode to socket if we were in run mode
        if (mode == MODE_RUN)
            setMode(MODE_SOCKET);

        Protocol.start(CMD_PONG, e->source);
        Protocol.end();
    }

    void onModeSet(const Event* e)
    {
        // TODO: invalid message, should we send an error? i dunno
        if (e->data->Size() <= 0)
            return;

        // TODO: is this event a valid mode?

        // First byte only is the mode
        setMode(e->data->At(0));

        onModeQuery(nullptr);
    }

    void onModeQuery(const Event* e)
    {
        Protocol.send(CMD_MODE_RESPONSE, mode);
    }

    /**
     * Send settings across serial
     */
    void onSettingsQuery(const Event* e)
    {
        Protocol.start(CMD_SETTINGS_RESPONSE, e->source);

        Protocol.write(MAX_BANKS);
        Protocol.write(PATCHES_PER_BANK);
        Protocol.write(DEFAULT_BANK);
        // DEFAULT PATCH
        Protocol.write(0);
        Protocol.write(MAX_NAME_LEN);
        Protocol.write(MAX_CUSTOM_CHARS);

        Protocol.end();
    }

    void onPatchQuery(const Event* e)
    {
        if (e->data->Size() != 2) {
            Protocol.send(CMD_PATCH_RESPONSE, 0xFF);
            return;
        }

        // First byte is bank index, second byte is patch
        byte bank = e->data->At(0);
        byte patch = e->data->At(1);

        // Find an excuse to quit due to errors
        if (bank >= MAX_BANKS || patch >= PATCHES_PER_BANK) {
            Protocol.send(CMD_PATCH_RESPONSE, 0xFF);
            return;
        }

        // We're screwed, just give it to them
        loadBank(bank);
        Patch* p = getBank()->getPatch(patch);

        // Start the response
        Protocol.start(CMD_PATCH_RESPONSE, e->source);

        // The bank and patch number
        Protocol.write(bank);
        Protocol.write(patch);

        // Send the name of the patch
        Protocol.write(p->getName(), MAX_NAME_LEN);

        // Send the channels
        unsigned short channels = p->getChannels();
        Protocol.write(&channels, 2, true);

        // Send the Active color
        Protocol.write(&p->getColorActive(), 3);
        // Send the Inactive color
        Protocol.write(&p->getColorInactive(), 3);

        Protocol.end();
    }

    void onPatchSet(const Event* e)
    {
        // Is the event for our device?
        if (! Protocol.has(e))
            return;

        // Compute the length of data we should expect
        // String length, two colors, 2 byte channel, 2 bytes for bank index and patch index
        const int packets = MAX_NAME_LEN + 6 + 4;

        if (e->data->Size() < packets) {
            // There was an error, invalid size!
            Protocol.error(CMD_PATCH_SET, 0x01);

            return;
        }

        byte bank = e->data->At(0);
        byte patch = e->data->At(1);

        unsigned int offset = 2;

        loadBank(bank);
        Patch* p = getBank()->getPatch(patch);

        // Pull the name of the patch and set it
        char name[MAX_NAME_LEN] = { 0 };
        (e->data->Slice(2, offset += MAX_NAME_LEN)).toCharArray(name);
        p->setName(name);

        // Get the channels since it's byte by byte we have to shift them
        unsigned short channels = ((unsigned short) e->data->At(offset++) << 8)  | (unsigned char) e->data->At(offset++);
        p->setChannels(channels);

        // Pull all 6 color bytes
        Color ic, ac;
        ac.setColor(e->data->At(offset), e->data->At(offset + 1), e->data->At(offset + 2));
        p->setActiveColor(ac);

        offset += 3;

        ic.setColor(e->data->At(offset), e->data->At(offset + 1), e->data->At(offset + 2));
        p->setInactiveColor(ic);

        // Save the bank to flash
        writeBank();
        loadBank(bank);

        // No error code
        Protocol.send(CMD_PATCH_RESPONSE, 0x00);
    }
    //endregion

    /*void ledRainbow(unsigned long cycleTime, int rainbowWidth)
    {
        // Displays a rainbow spread over a few LED's (numRGBLeds), which shifts in hue.
        // The rainbow can be wider then the real number of LED's.
        unsigned long time = millis()-startTime;
        unsigned long colorShift = (360*time/cycleTime)%360; // this color shift is like the hue slider in Photoshop.

        for(unsigned int led = 0; led < PATCHES_PER_BANK; led++){ // loop over all LED's
            int hue = ((led)*360/(rainbowWidth-1)+colorShift)%360; // Set hue from 0 to 360 from first to last led and shift the hue
            ShiftPWM.SetHSV(led, hue, 255, 255); // write the HSV values, with saturation and value at maximum
        }
    }*/
};

static MidiController Controller;

void onPingHandler(Event* data) {
    Controller.onPing(data);
}

void onModeSetHandler(Event* data) {
    Controller.onModeSet(data);
}

void onModeQueryHandler(Event* data) {
    Controller.onModeQuery(data);
}

void onSettingsQueryHandler(Event* data) {
    Controller.onSettingsQuery(data);
}

void onPatchQueryHandler(Event* data) {
    Controller.onPatchQuery(data);
}

void onPatchSetHandler(Event* data) {
    Controller.onPatchSet(data);
}



#endif //MIDI_CONTROLLER_MIDICONTROLLER_H
