#ifndef _INPUTMANAGER_h
#define _INPUTMANAGER_h

#include "IO/Input.h"

namespace IO
{
	class InputManager
	{
	protected:
		Input** inputs = nullptr;

		size_t size = 0;
		size_t max = 0;

		boolean disabled = false;

	public:
//		InputManager() { }

		virtual ~InputManager()
		{
			for (size_t i = 0; i < size; ++i)
			{
				delete inputs[i];
				delete[] inputs;
			}
		}

		virtual void init(unsigned int size)
		{
			this->max = size;
			inputs = new Input*[size];
		}

		virtual InputManager* add(Input* input)
		{
			if (size < max)
				inputs[size++] = input;
			return this;
		}

		virtual Input* getByPin(int pin)
		{
			for (size_t i = 0; i < size; ++i)
			{
				if (inputs[i]->getPin() == pin)
				{
					return inputs[i];
				}
			}

			return nullptr;
		}

		virtual Input* getByIndex(size_t index)
		{
			if (index >= size)
				return inputs[index];
			return nullptr;
		}

		virtual void handle()
		{
			if (this->disabled)
				return;

			for (size_t i = 0; i < size; ++i)
			{
				inputs[i]->handle();
			}
		}

		virtual InputManager* setDisabled(boolean state)
		{
			this->disabled = state;

			return this;
		}
	};

	static InputManager Inputs;
}

#endif
