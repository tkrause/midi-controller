// Button.h
#ifndef _BUTTON_h
#define _BUTTON_h

#include "Input.h"
#include "Arduino.h"

using namespace IO;

class Button : public Input
{
public:
	explicit Button(int pin, InputMode mode = PULL_DOWN) : Input(pin, mode)
	{
	}
};

#endif