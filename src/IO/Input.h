#ifndef _INPUT_h
#define _INPUT_h

#include "Arduino.h"

namespace IO
{
	class Input;
	typedef void (*Callback)(Input*);

	enum InputMode
	{
		PULL_UP, PULL_DOWN
	};

	class Input
	{
	protected:
		int pin;
		int state;

		bool allowLongPress = false;

		unsigned long threshold = 1000;
		unsigned long timer = 0;

		Callback onChange = nullptr;
		Callback onPressed = nullptr;
		Callback onReleased = nullptr;
		Callback onLongPress = nullptr;

		InputMode mode;

	public:
		Input(int pin, InputMode mode = PULL_DOWN)
		{
			init(pin, mode);
		}

		virtual ~Input() { }

		/**
		 * Initialize the input
		 */
		void init(int pin, InputMode mode)
		{
			this->pin = pin;
			this->mode = mode;

			// Set the starting state of the input
			this->state = mode == PULL_DOWN ? LOW : HIGH;

			pinMode(this->pin, INPUT);
		}

		/**
		 * Get the pin for this input
		 */
		virtual int getPin() const
		{
			return this->pin;
		}

		/**
		 * Get the input mode
		 */
		virtual InputMode getMode() const
		{
			return this->mode;
		}

		/**
		 * Get the input state
		 */
		virtual int getState() const
		{
			return this->state;
		}

		virtual bool isPressed() const
		{
			return (mode == PULL_DOWN && state == HIGH) || (mode == PULL_UP && state == LOW);
		}

		virtual bool isReleased() const
		{
			return !isPressed();
		}

		/**
		 * Is long press enabled?
		 */
		virtual bool isLongPressEnabled() const
		{
			return this->allowLongPress;
		}

		/**
		 * Set the onChange callback
		 */
		virtual Input* setOnChange(Callback callback)
		{
			this->onChange = callback;

			return this;
		}

		/**
		 * Set the onChange callback
		 */
		virtual Input* setOnPressed(Callback callback)
		{
			this->onPressed = callback;

			return this;
		}

		/**
		 * Set the onChange callback
		 */
		virtual Input* setOnReleased(Callback callback)
		{
			this->onReleased = callback;

			return this;
		}
		
		/**
		 * Set the long press callback
		 */
		virtual Input* setOnLongPress(Callback callback)
		{
			this->onLongPress = callback;

			return this;
		}

		/**
		 * Set the threshold in milliseconds for how long to consider a long press
		 */
		virtual Input* setThreshold(unsigned long threshold)
		{
			this->threshold = threshold;

			return this;
		}

		/**
		 * Set Long Press Enabled
		 */
		virtual Input* setLongPressEnabled(bool status)
		{
			this->allowLongPress = status;

			return this;
		}

		/**
		 * Check if there is a long press
		 */
		virtual bool hasLongPress()
		{
			// If long press is not enabled 
			// or if these state isn't default for the mode
			// bail out
			if (!isLongPressEnabled() || (mode == PULL_DOWN && state == LOW) || (mode == PULL_UP && state == HIGH))
				return false;

			// Get time and diff with timer
			return (millis() - this->timer) >= this->threshold;
		}

		virtual void handle()
		{
			// Read the new state
			int newState = digitalRead(this->pin);
			// If the state is the same
			if (newState == this->state)
			{
				// If there is a long press and we have a callback
				// execute the call back
				if (hasLongPress() && onLongPress != nullptr)
					onLongPress(this);

				// Bail out, nothing more to do
				return;
			}

			// If we allow long press set the timer at the start of the 
			// press
			if (isLongPressEnabled())
				this->timer = millis();

			// The state has changed so set the new state
			this->state = newState;
			// If we have a onChange callback execute it
			if (onChange != nullptr)
				onChange(this);

			// The input / button is pressed
			if (onPressed != nullptr && isPressed())
				onPressed(this);

			// The input / button is released
			if (onReleased != nullptr && isReleased())
				onReleased(this);

		}
	};
}

#endif
