#include <Arduino.h>
#include "U8glib.h"

// 52, 51, 53, 35, 36
U8GLIB_SH1106_128X64 u8g(39, 38, 37, 36, 35); // D0=13, D1=11, CS=10, DC=9, Reset=8
U8GLIB_SH1106_128X64 u8g2(39, 38, 40, 36, 35); // D0=13, D1=11, CS=10, DC=9, Reset=8

const uint8_t brainy_bitmap[] PROGMEM = {
        0x00, 0x00, 0x03, 0xB0, 0x00, 0x00, 0x00, 0x00, 0x07, 0xFC, 0x00, 0x00, 0x00, 0x00, 0x0C, 0x46,
        0x00, 0x00, 0x00, 0x00, 0xFC, 0x47, 0xC0, 0x00, 0x00, 0x01, 0xCE, 0x4C, 0x60, 0x00, 0x00, 0x03,
        0x02, 0x58, 0x30, 0x00, 0x00, 0x03, 0x02, 0x58, 0x10, 0x00, 0x00, 0x02, 0x02, 0x58, 0x18, 0x00,
        0x00, 0x03, 0x06, 0x4C, 0x18, 0x00, 0x00, 0x07, 0x04, 0x44, 0x18, 0x00, 0x00, 0x0D, 0x80, 0x40,
        0x3C, 0x00, 0x00, 0x09, 0xC0, 0x40, 0xE6, 0x00, 0x00, 0x18, 0x78, 0x47, 0xC2, 0x00, 0x00, 0x18,
        0x0C, 0x4E, 0x02, 0x00, 0x00, 0x1F, 0x86, 0x4C, 0x7E, 0x00, 0x00, 0x0E, 0xC6, 0xE8, 0xEE, 0x00,
        0x00, 0x18, 0x43, 0xF8, 0x82, 0x00, 0x00, 0x10, 0x06, 0x4C, 0x03, 0x00, 0x00, 0x30, 0x0C, 0x46,
        0x01, 0x00, 0x00, 0x30, 0x18, 0x46, 0x01, 0x00, 0x00, 0x10, 0x18, 0x43, 0x03, 0x00, 0x00, 0x18,
        0x10, 0x43, 0x03, 0x00, 0x00, 0x1C, 0x70, 0x41, 0x86, 0x00, 0x00, 0x0F, 0xE0, 0x40, 0xFE, 0x00,
        0x00, 0x09, 0x1E, 0x4F, 0x06, 0x00, 0x00, 0x08, 0x30, 0x43, 0x86, 0x00, 0x00, 0x0C, 0x20, 0x41,
        0x86, 0x00, 0x00, 0x06, 0x60, 0x40, 0x8C, 0x00, 0x00, 0x07, 0x60, 0x40, 0xB8, 0x00, 0x00, 0x01,
        0xE0, 0x41, 0xF0, 0x00, 0x00, 0x00, 0x38, 0xE3, 0x00, 0x00, 0x00, 0x00, 0x0F, 0xBE, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x1F, 0xCF, 0x82, 0x0C, 0x86, 0x46, 0x1F, 0xEF, 0xC3, 0x0C,
        0xC6, 0xEE, 0x1C, 0xEC, 0xC7, 0x0C, 0xE6, 0x7C, 0x1C, 0xED, 0x8D, 0x8C, 0xFE, 0x38, 0x1C, 0xED,
        0x8D, 0xCC, 0xDE, 0x38, 0x1D, 0xCD, 0xDF, 0xCC, 0xCE, 0x38, 0x1F, 0x8C, 0xF8, 0xEC, 0xC6, 0x38,
        0x1F, 0xEC, 0x08, 0x0C, 0xC2, 0x18, 0x1C, 0xEC, 0x00, 0xC0, 0x00, 0x00, 0x1C, 0xFD, 0xFB, 0xC0,
        0x00, 0x00, 0x1C, 0xFC, 0x63, 0x00, 0x00, 0x00, 0x1C, 0xEC, 0x63, 0xC0, 0x00, 0x00, 0x1F, 0xEC,
        0x60, 0xC0, 0x00, 0x00, 0x1F, 0xCC, 0x63, 0xC0, 0x00, 0x00, 0x1F, 0x0C, 0x63, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x28, 0x2B, 0x4F, 0x67,
        0x42, 0x38, 0x7B, 0xEA, 0x86, 0xB2, 0x28, 0xC7,

};

void draw() {
    u8g.drawBitmapP( 76, 5, 6, 50, brainy_bitmap);  // put bitmap
    u8g.setFont(u8g_font_unifont);  // select font
    u8g.drawStr(0, 30, "Temp: ");  // put string of display at position X, Y
    u8g.drawStr(0, 50, "Hum: ");
    u8g.setPrintPos(44, 30);  // set position
    u8g.print(80, 0);  // display temperature from DHT11
    u8g.drawStr(60, 30, "c ");
    u8g.setPrintPos(44, 50);
    u8g.print(20, 0);  // display humidity from DHT11
    u8g.drawStr(60, 50, "% ");
}

void setup() {
//    u8g.setPrintPos(0,0);
//    u8g.print(20, 0);
//
//    delay(5000);
}

void loop() {
//    u8g.firstPage();
//    do {
//        u8g.setFont(u8g_font_unifont);
//        u8g.drawStr(0, 15, "Hello World");
//    } while(u8g.nextPage());
//
//
//
//    delay(5000);
//    DHT.read11(dht_apin);  // Read apin on DHT11

    u8g.firstPage();
    do {
        draw();
    } while( u8g.nextPage() );

//    u8g2.firstPage();
//    do {
//        u8g2.setFont(u8g_font_unifont);
//        u8g2.drawStr(0, 40, "Hello World - 2");
//    } while(u8g2.nextPage());

    delay(5000);  // Delay of 5sec before accessing DHT11 (min - 2sec)
}

/*
#include <Arduino.h>
#include "MidiController.h"

const char VENDOR_ID[] = { 0x7D };
const unsigned int VENDOR_ID_SIZE = 1;//sizeof(VENDOR_ID) / sizeof(VENDOR_ID[0]);

void patchChangeHandler(Input*);
void bankUpHandler(Input*);
void bankDownHandler(Input*);

void POST()
{
    Serial.println("POST");
    for (unsigned int i = 0; i < PATCHES_PER_BANK; ++i) {
        ShiftPWM.SetAll(0);

        ShiftPWM.SetRGB(i, 255, 0, 0);
        delay(200);
        ShiftPWM.SetRGB(i, 0, 255, 0);
        delay(200);
        ShiftPWM.SetRGB(i, 0, 0, 255);
        delay(200);
    }

    ShiftPWM.SetAll(0);
}

void setup()
{
    lcd.begin(16, 2);

    // Initialize components
    Serial.begin(9600);
    Wire.begin();

    ShiftPWM.SetAmountOfRegisters(3);
    ShiftPWM.SetPinGrouping(1);
    ShiftPWM.Start(75, 255);

    // Make sure our hardware works
    lcd.println("Booting...");

    Controller.init();

//        this->POST();

//    lcd.print("Wiping EEPROM...");
//    Helper::clear(STORAGE_DISK, STORAGE_SIZE);

    // SETUP A TEST PATCH
    */
/*Patch patch;
    Color blue(0, 0, 255);
    patch.setName("Drive")
            ->setActiveColor(Color(0, 255, 0))
            ->setInactiveColor(Color(255, 0, 0))
            ->setChannel(0, true);

    Controller.getBank()->setPatch(0, patch);

    Patch patch2;
    patch2.setName("Drive2")
            ->setFlag(PATCH_FLAG_IS_PROGRAM_CHANGE, true)
            ->setActiveColor(Color(0, 255, 0))
            ->setInactiveColor(Color(255, 0, 0))
            ->setChannel(0, true);

    Controller.getBank()->setPatch(1, patch2);

    Patch patch3;
    patch3.setName("Drive3")
            ->setFlag(PATCH_FLAG_IS_PROGRAM_CHANGE, true)
            ->setActiveColor(blue)
            ->setInactiveColor(Color(255, 0, 0))
            ->setChannel(0, true);

    Controller.getBank()
            ->setPatch(2, patch3)
            ->setName("Bank 1");

    Controller.writeBank();*//*


    // Load the configuration for the default bank
    Controller.start();

    //	 Patch count + bank up and down
    Inputs.init(PATCHES_PER_BANK + 2);

    const int patch_btn_end_pin = PATCH_BTN_START_PIN + PATCHES_PER_BANK;
    for (int i = PATCH_BTN_START_PIN; i < patch_btn_end_pin; ++i)
    {
        Inputs.add(
                (new Input(i))->setOnChange(&patchChangeHandler)
        );
    }

    Inputs.add(
            (new Input(BANK_UP_PIN))->setOnPressed(&bankUpHandler)
    )->add(
            (new Input(BANK_DOWN_PIN))->setOnPressed(&bankDownHandler)
    );
}

void loop()
{
    Inputs.handle();
    Controller.loop();
}

void patchChangeHandler(Input* object)
{
    if (! object->isPressed())
        return;

    Controller.togglePatch(object->getPin() - PATCH_BTN_START_PIN);
}

// TODO: On bank change, save the current state
// of all the patches and restore it when we go back to the bank they match
// currently they are lost
void bankUpHandler(Input* object)
{
    lcd.clear();
    Controller.nextBank()->display(lcd);
}

void bankDownHandler(Input* object)
{
    lcd.clear();
    Controller.prevBank()->display(lcd);
}*/
