#ifndef _HELPER_h
#define _HELPER_h

//#include <EEPROM.h>
#include <Arduino.h>
#include <Wire.h>

class Helper
{
public:
	template <class T>
	static int write(int device, unsigned int start, const T& value)
	{
		const byte* p = (const byte*)(const void*)&value;
		unsigned int i;
		for (i = 0; i < sizeof(value); ++i)
			updateEEPROM(device, start++, *p++);
//			EEPROM.update(start++, *p++);
//
		return i;
	}

	template <class T>
	static int read(int device, unsigned int start, T& value)
	{
		byte* p = (byte*)(void*)& value;
		unsigned int i;

		for (i = 0; i < sizeof(value); ++i) {
			*p++ = readEEPROM(device, start++);
		}
//			*p++ = EEPROM.read(start++);
		return i;
	}

	static void updateEEPROM(int device, unsigned int address, byte data)
	{
		byte d = readEEPROM(device, address);
		if (d != data)
			writeEEPROM(device, address, data);
	}

	static void writeEEPROM(int device, unsigned int address, byte data)
	{
		Wire.beginTransmission(device);
		Wire.write((int)(address >> 8));
		Wire.write((int)(address & 0xFF));
		Wire.write(data);
		Wire.endTransmission();

		delay(5);
	}

	static byte readEEPROM(int device, unsigned int address)
	{
		Wire.beginTransmission(device);
		Wire.write((int)(address >> 8));
		Wire.write((int)(address & 0xFF));
		Wire.endTransmission();

		Wire.requestFrom(device, 1);

        byte data = (byte) Wire.read();
		return data;
	}

	static void clear(unsigned int device, unsigned long size)
    {
        for (unsigned long i = 0; i < size; ++i) {
			updateEEPROM(device, i, 0);
        }
    }
};

#endif

