#ifndef MIDI_CONTROLLER_COLOR_H
#define MIDI_CONTROLLER_COLOR_H

#include <Arduino.h>

class Color
{
protected:
    // Default to black
    byte red, green, blue;

public:
    Color() { }
    Color(byte red, byte green, byte blue) : red(red), green(green), blue(blue) { }

    const Color* setColor(byte red)
    {
        this->setColor(red, green, blue);

        return this;
    }

    const Color* setColor(byte red, byte green)
    {
        this->setColor(red, green, blue);

        return this;
    }

    const Color* setColor(byte red, byte green, byte blue)
    {
        this->red = red;
        this->green = green;
        this->blue = blue;

        return this;
    }

    byte getRed() const
    {
        return this->red;
    }

    byte getBlue() const
    {
        return this->blue;
    }

    byte getGreen() const
    {
        return this->green;
    }

    Color& setRed(byte red)
    {
        this->red = red;

        return *this;
    }

    Color& setBlue(byte blue)
    {
        this->blue = blue;
        return *this;
    }

    Color& setGreen(byte green)
    {
        this->green = green;
        return *this;
    }

    Color getInverse() const
    {
        Color inverse;
        inverse.setColor(
            255 - this->getRed(),
            255 - this->getGreen(),
            255 - this->getBlue()
        );

        return inverse;
    }
};

#endif //MIDI_CONTROLLER_COLOR_H
