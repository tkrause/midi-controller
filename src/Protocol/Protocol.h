#ifndef MIDI_CONTROLLER_PROTOCOL_H
#define MIDI_CONTROLLER_PROTOCOL_H

#include <HardwareSerial.h>
#include <ArduinoJson.h>
#include <Arduino.h>
#include "Buffer.h"
#include "Commands.h"

extern const char VENDOR_ID[];
extern const unsigned int VENDOR_ID_SIZE;

using namespace Internals;

#define PROTO_START 0xF0
#define PROTO_END 0xF7

struct Event
{
    const byte source;
    const byte dest;
    const byte command;
    const Buffer* data;

    Event(const byte& source, const byte& dest, const byte& command, const Buffer* data)
            : source(source), dest(dest), command(command), data(data) { };
};

typedef void (*Callback)(Event*);

struct Handler {
    byte command;
    Callback callback;

    Handler(byte command, Callback callback) : command(command), callback(callback) { }
};

/**
 * Serial communication protocol
 */
class CProtocol
{
protected:
    HardwareSerial* socket = nullptr;
    bool capturing = false;
    Buffer buffer;

    Handler** handlers = nullptr;

    byte id = 0x00;

    size_t size = 0;
    size_t max = 20;

    void handleBuffer();

public:
    virtual ~CProtocol();

    CProtocol* begin(HardwareSerial& socket, const byte& id);
    CProtocol* begin(HardwareSerial& socket, const byte& id, unsigned int size);
    HardwareSerial& device();
    CProtocol* process();
    CProtocol* flush();

    CProtocol* on(char command, Callback callback);
    CProtocol* on(int command, Callback callback);

    CProtocol* start(const char& command, const byte& device = 0xFF);
    CProtocol* start(const int& command, const byte& device = 0xFF) { return start((char) command, device); };

    CProtocol* send(const int& command) { return send((char) command); }
    CProtocol* send(const char& command);
    CProtocol* send(const char& command, const void* data);
    CProtocol* send(const char& command, const int& data);

//    CProtocol* write(const & start)
    CProtocol* write(const unsigned int& start) {
        socket->write(start);
        return this;
    }

    CProtocol* write(const int& start) {
        socket->write(start);
        return this;
    }

    CProtocol* write(const void* start, bool reverse = false);
    CProtocol* write(const void* start, const int& count, bool reverse = false);
    CProtocol* write(const void* start, const unsigned& count, bool reverse = false);

//    CProtocol* error(const int& command, const String& data);
    CProtocol* error(const int& command, const int& data);

    CProtocol* end();

    bool has(const Event* event);
};

static CProtocol Protocol;

#endif //MIDI_CONTROLLER_PROTOCOL_H
