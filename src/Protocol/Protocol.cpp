#include "Protocol.h"

CProtocol::~CProtocol()
{
    for (size_t i = 0; i < size; ++i)
    {
        delete handlers[i];
        delete[] handlers;
    }
}

CProtocol *CProtocol::begin(HardwareSerial &socket, const byte& id)
{
    return this->begin(socket, 5, id);
}

CProtocol *CProtocol::begin(HardwareSerial& socket, const byte& id, unsigned int size)
{
    this->socket = &socket;
    this->max = size;
    this->id = id;
    handlers = new Handler*[size];

    return this;
}

HardwareSerial &CProtocol::device() {
    return *socket;
}

CProtocol *CProtocol::process()
{
    while (socket->available() > 0) {
        auto b = (byte) socket->read();
        // If we find the start of a message, start capturing
        if (! this->capturing) {
            this->capturing = b == (byte) PROTO_START;

            continue;
        }

        // If we get here, we're capturing
        if (b == (byte) PROTO_END) {
            handleBuffer();

            continue;
        }

        buffer.PushBack(b);
    }

    return this;
}

void CProtocol::handleBuffer()
{
    capturing = false;

    auto prefix = buffer.Slice(0, VENDOR_ID_SIZE);

    // The message is for us, continue parsing
    if (prefix == VENDOR_ID) {
        auto command = buffer.At(VENDOR_ID_SIZE);
        auto source = buffer.At(VENDOR_ID_SIZE + 1);
        auto destination = buffer.At(VENDOR_ID_SIZE + 2);

        buffer.Erase(0, VENDOR_ID_SIZE + 3);
        for (size_t i = 0; i < size; ++i) {
            // If this handler wants to accept this command event
            // trigger it
            if (handlers[i]->command == command) {
//                Serial.print("in command loop");
                Event event(source, destination, command, &buffer);
                handlers[i]->callback(&event);
            }
        }
    }

    // Truncate the buffer
    buffer.Clear();
}

CProtocol* CProtocol::flush()
{
    if (capturing) {
        handleBuffer();
    }

    return this;
}

CProtocol* CProtocol::on(int command, Callback callback)
{
    return this->on((char) command, callback);
}

CProtocol* CProtocol::on(char command, Callback callback)
{
    if (size < max)
        handlers[size++] = new Handler(command, callback);

    return this;
}

CProtocol* CProtocol::start(const char& command, const byte& device)
{
    write((char) PROTO_START);
    write(VENDOR_ID);
    write(command);
    write(id);
    write(device);

    return this;
}

CProtocol *CProtocol::send(const char &command)
{
    start(command);
    end();

    return this;
}

CProtocol *CProtocol::send(const char &command, const void* data)
{
    start(command);
    write(data);
    end();

    return this;
}
CProtocol *CProtocol::send(const char &command, const int& data)
{
    start(command);
    write(data);
    end();

    return this;
}

CProtocol *CProtocol::write(const void *start, bool reverse) {
    auto p = reinterpret_cast<const byte*>(start);
    write(start, (int) strlen(reinterpret_cast<const char*>(p)), reverse);

    return this;
}

CProtocol *CProtocol::write(const void *start, const unsigned int &count, bool reverse) {
    auto p = reinterpret_cast<const byte*>(start);

    if (reverse) {
        for (unsigned int i = count - 1; i >= 0 ; --i) {
            socket->write(p[i]);
        }
    } else {
        for (unsigned int i = 0; i < count; ++i) {
            socket->write(p[i]);
        }
    }

    return this;
}


CProtocol *CProtocol::write(const void* start, const int &count, bool reverse) {
    auto p = reinterpret_cast<const byte*>(start);

    if (reverse) {
        for (int i = count - 1; i >= 0 ; --i) {
            socket->write(p[i]);
        }
    } else {
        for (int i = 0; i < count; ++i) {
            socket->write(p[i]);
        }
    }

    return this;
}

CProtocol* CProtocol::end()
{
    write((char) PROTO_END);

    return this;
}

CProtocol *CProtocol::error(const int &command, const int &data) {
    start((char) CMD_ERROR);
    write((char) command);
    write(data);
//    sendData((char) command);
//    sendData(data);
    end();

    return this;
}

bool CProtocol::has(const Event *event) {
    return event->dest == 0xFF || event->dest == 0x00 || event->dest == id;
}